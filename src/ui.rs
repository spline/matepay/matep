// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use rocket::{
    catch,
    form::Form,
    http::Status,
    request::FromRequest,
    response::status::Custom,
    response::Redirect,
    serde::json::Json,
    uri, {get, post},
};

use std::{collections::HashMap, iter::repeat};

use rocket_dyn_templates::Template;

use itertools::Itertools;

use serde::{Deserialize, Serialize};

use log::info;

use qrcode::render::svg;
use qrcode::QrCode;

use crate::{
    database::Db,
    endpoints::{
        self, handle_database_error, Account, AddItemRequest, CentralBankTransfer, CentralBankUser,
        Item, Location, MatePayResult, PayoutRequest, Transfer, UpdateItemRequest,
    },
    MATEPAY_CENTRAL_BANK_USER,
};

use spline_oauth::routes::AuthenticatedUser;

#[catch(402)]
pub async fn catch_payment_required() -> Template {
    payment_required()
}

#[catch(403)]
pub async fn catch_forbidden() -> Template {
    Template::render("forbidden", HashMap::<String, String>::new())
}

#[get("/payment_required")]
pub fn payment_required() -> Template {
    Template::render("payment_required", HashMap::<String, String>::new())
}

#[get("/")]
pub async fn index() -> Template {
    Template::render("index", HashMap::<String, String>::new())
}

pub fn error_page(status: Status, summary: Option<&str>) -> Custom<Template> {
    if status == Status::PaymentRequired {
        return Custom(status, payment_required());
    }

    #[derive(Serialize)]
    struct Context {
        title: String,
        summary: Option<String>,
    }

    let title = status.reason().unwrap_or("Error").to_string();

    Custom(
        status,
        Template::render(
            "error",
            Context {
                title,
                summary: summary.map(ToOwned::to_owned),
            },
        ),
    )
}

pub fn default_error_page(status: Status) -> Custom<Template> {
    error_page(status, None)
}

#[get("/home")]
pub async fn home(
    db: Db,
    auth_db: spline_oauth::db::Db,
    user: AuthenticatedUser,
) -> MatePayResult<Template> {
    let name = user
        .get_display_name(&auth_db)
        .await
        .map_err(handle_database_error)?;
    if !db
        .check_account_exists(user.username.clone())
        .await
        .map_err(handle_database_error)?
    {
        db.create_account(user.username.clone(), name.unwrap_or(user.username.clone()))
            .await
            .map_err(handle_database_error)?;
    } else {
        if let Some(name) = name {
            db.add_account_display_name(user.username.clone(), name)
                .await
                .map_err(handle_database_error)?;
        }
    }
    let account = endpoints::account(db, user).await?;

    let is_bank = account.username == MATEPAY_CENTRAL_BANK_USER;

    #[derive(Serialize)]
    struct Context {
        account: Account,
        is_bank: bool,
    }

    Ok(Template::render(
        "home",
        Context {
            account: account.into_inner(),
            is_bank,
        },
    ))
}

#[get("/transactions")]
pub async fn transactions(db: Db, db2: Db, user: AuthenticatedUser) -> MatePayResult<Template> {
    let Json(transactions) = endpoints::transactions(db, user).await?;

    #[derive(Serialize)]
    struct GuiTransaction {
        timestamp: i64,
        sender: String,
        receiver: String,
        amount: i64,
        items: Vec<Item>,
    }

    let mut gui_transactions = Vec::new();
    for transaction in transactions {
        let mut gui_transaction = GuiTransaction {
            timestamp: transaction.timestamp,
            sender: transaction.sender,
            receiver: transaction.receiver,
            amount: transaction.amount,
            items: Vec::new(),
        };

        for item in transaction.items {
            let item = db2.get_item(item).await.map_err(handle_database_error)?;
            if !gui_transaction.items.contains(&item) {
                gui_transaction.items.push(item);
            }
        }
        gui_transactions.push(gui_transaction);
    }

    #[derive(Serialize)]
    struct Context {
        transactions: Vec<GuiTransaction>,
    }

    Ok(Template::render(
        "transactions",
        Context {
            transactions: gui_transactions,
        },
    ))
}

fn selected_items(form: &HashMap<String, String>) -> Option<Vec<(i32, u16)>> {
    let ids: Vec<i32> = form
        .iter()
        .filter(|(_, v)| *v == "on")
        .map(|(k, _)| k.replace("item-", ""))
        .flat_map(|s| s.parse::<i32>())
        .sorted()
        .collect();

    let mut out = Vec::new();
    for id in ids {
        let quantity = form
            .get(&format!("quantity-{id}"))
            .unwrap_or(&"1".to_string())
            .parse::<u16>();

        match quantity {
            Ok(quantity) => {
                out.push((id, quantity));
            }
            Err(e) => {
                info!("Quantity was not a number: {e}");
                return None;
            }
        }
    }
    Some(out)
}

#[test]
fn test_parse_selected_items() {
    let form = HashMap::from_iter(
        [("item-1", "on"), ("item-2", "off"), ("item-3", "on")]
            .iter()
            .map(|(k, v)| (k.to_string(), v.to_string()))
            .collect::<Vec<_>>(),
    );
    let items = selected_items(&form);
    assert_eq!(items.unwrap(), [(1, 1), (3, 1)]);
}

#[get("/direct-transfer")]
pub async fn direct_transfer(user: AuthenticatedUser, db: Db) -> MatePayResult<Template> {
    let totp_enabled = db
        .totp_enabled(user.username)
        .await
        .map_err(handle_database_error)?;

    #[derive(Serialize, Deserialize)]
    struct Context {
        totp_enabled: bool,
    }

    Ok(Template::render(
        "direct-transfer",
        Context { totp_enabled },
    ))
}

#[post("/do-direct-transfer", data = "<transfer>")]
pub async fn do_direct_transfer(
    db: Db,
    user: AuthenticatedUser,
    transfer: Form<endpoints::DirectTransfer>,
) -> MatePayResult<Redirect> {
    endpoints::direct_transfer(db, user, Json(transfer.into_inner())).await?;

    Ok(Redirect::to(uri!(transactions())))
}

#[post("/do-transfer", data = "<transfer>")]
pub async fn do_transfer(
    db: Db,
    user: AuthenticatedUser,
    transfer: Form<HashMap<String, String>>,
) -> MatePayResult<Redirect> {
    let items = selected_items(&transfer);

    let location = if let Some(location) = transfer.get("location") {
        location.clone()
    } else {
        return Err(error_page(
            Status::UnprocessableEntity,
            Some("location field must be sent in form"),
        ));
    };

    let items = match items {
        None => {
            return Err(error_page(
                Status::UnprocessableEntity,
                Some("quantity was not a number"),
            ));
        }
        Some(items) => items,
    };

    info!(
        "{} bought items {:?} from {}",
        user.username.clone(),
        items,
        location
    );

    let transfer = Transfer {
        receiver: location.clone(),
        bought_items: items
            .into_iter()
            .flat_map(|(id, quantity)| repeat(id).take(quantity as usize))
            .collect(),
        totp_token: transfer.get("totp_secret").cloned(),
    };

    let status = endpoints::internal_item_transfer(&db, user, &transfer).await;
    match status {
        Ok(()) => Ok(Redirect::to(uri!(transactions))),
        Err(Custom(status, body)) => match status {
            s if s == Status::Forbidden => Ok(Redirect::to(uri!(get_items(location)))),
            _ => Err(Custom(status, body)),
        },
    }
}

#[get("/items/<location>")]
pub async fn get_items(
    db: Db,
    location: String,
    user: AuthenticatedUser,
) -> MatePayResult<Template> {
    let own_location = user.username == location;
    let totp_enabled = db
        .totp_enabled(user.username)
        .await
        .map_err(handle_database_error)?;

    let items = endpoints::items(db, location.clone()).await?.into_inner();

    #[derive(Serialize)]
    struct Context {
        items: Vec<Item>,
        location: String,
        own_location: bool,
        totp_enabled: bool,
    }

    Ok(Template::render(
        "items",
        Context {
            items,
            location,
            own_location,
            totp_enabled,
        },
    ))
}

#[get("/locations")]
pub async fn locations(db: Db) -> MatePayResult<Template> {
    let locations = endpoints::locations(db).await?.into_inner();

    #[derive(Serialize)]
    struct Context {
        locations: Vec<Location>,
    }

    Ok(Template::render("locations", Context { locations }))
}

#[get("/add-item")]
pub async fn add_item(user: AuthenticatedUser) -> Template {
    #[derive(Serialize)]
    struct Context {
        username: String,
    }

    Template::render(
        "add_item",
        Context {
            username: user.username,
        },
    )
}

#[post("/do-add-item", data = "<item>")]
pub async fn do_add_item(
    db: Db,
    user: AuthenticatedUser,
    item: Form<AddItemRequest>,
) -> MatePayResult<Redirect> {
    endpoints::add_item(db, user, Json(item.into_inner())).await?;

    Ok(Redirect::to(uri!(home)))
}

#[get("/update-item/<id>")]
pub async fn update_item(db: Db, id: i32, user: AuthenticatedUser) -> MatePayResult<Template> {
    let item = endpoints::item(db, id).await?;

    #[derive(Serialize)]
    struct Context {
        name: String,
        username: String,
        id: i32,
    }

    Ok(Template::render(
        "update_item",
        Context {
            name: item.name.clone(),
            username: user.username.clone(),
            id,
        },
    ))
}

#[post("/do-update-item/<id>", data = "<item>")]
pub async fn do_update_item(
    db: Db,
    user: AuthenticatedUser,
    id: i32,
    item: Form<UpdateItemRequest>,
) -> MatePayResult<Redirect> {
    endpoints::update_item(db, user, id, Json(item.into_inner())).await?;

    Ok(Redirect::to(uri!(home)))
}

#[post("/do-deposit", data = "<transfer>")]
pub async fn do_deposit(
    db: Db,
    bank: CentralBankUser,
    transfer: Form<CentralBankTransfer>,
) -> MatePayResult<Redirect> {
    endpoints::deposit(db, bank, Json(transfer.into_inner())).await?;

    Ok(Redirect::to(uri!(transactions)))
}

#[post("/do-payout", data = "<transfer>")]
pub async fn do_payout(
    db: Db,
    bank: CentralBankUser,
    transfer: Form<PayoutRequest>,
) -> MatePayResult<Template> {
    let response = endpoints::payout(db, bank, Json(transfer.into_inner())).await?;

    #[derive(Serialize)]
    struct Context {
        amount: i64,
    }

    Ok(Template::render(
        "payout_done",
        Context {
            amount: response.into_inner().amount,
        },
    ))
}

#[get("/deposit")]
pub async fn deposit(bank: CentralBankUser, db: Db) -> MatePayResult<Template> {
    #[derive(Serialize)]
    struct Context {
        totp_enabled: bool,
    }

    Ok(Template::render(
        "deposit",
        Context {
            totp_enabled: db
                .totp_enabled(bank.user.username)
                .await
                .map_err(handle_database_error)?,
        },
    ))
}

#[get("/payout")]
pub async fn payout(db: Db, bank: CentralBankUser) -> MatePayResult<Template> {
    #[derive(Serialize)]
    struct Context {
        totp_enabled: bool,
    }

    Ok(Template::render(
        "payout",
        Context {
            totp_enabled: db
                .totp_enabled(bank.user.username)
                .await
                .map_err(handle_database_error)?,
        },
    ))
}

#[derive(Serialize)]
pub struct OauthUrl {
    url: String,
}

#[post("/totp")]
pub async fn setup_totp(db: Db, user: AuthenticatedUser) -> MatePayResult<Template> {
    let secret = endpoints::enable_totp(db, user).await?;
    let code = QrCode::new(secret.uri.as_bytes()).expect("Payload should not be too large");
    let qrcode = code.render::<svg::Color>().build();

    #[derive(Serialize)]
    struct Context {
        qrcode: String,
        uri: String,
    }

    Ok(Template::render(
        "totp_result",
        Context {
            qrcode,
            uri: secret.uri.clone(),
        },
    ))
}

#[get("/totp")]
pub async fn totp(db: Db, user: AuthenticatedUser) -> MatePayResult<Template> {
    let totp_enabled = db
        .totp_enabled(user.username)
        .await
        .map_err(handle_database_error)?;

    #[derive(Serialize)]
    struct Context {
        totp_enabled: bool,
    }

    Ok(Template::render("totp", Context { totp_enabled }))
}

#[get("/settings")]
pub async fn settings(_user: AuthenticatedUser) -> MatePayResult<Template> {
    #[derive(Serialize)]
    struct Context {}

    Ok(Template::render("settings", Context {}))
}

pub struct Host<'r>(Option<&'r str>);

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Host<'r> {
    type Error = ();

    async fn from_request(
        request: &'r rocket::Request<'_>,
    ) -> rocket::request::Outcome<Self, Self::Error> {
        rocket::request::Outcome::Success(Host(request.headers().get("host").next()))
    }
}

#[get("/register-app")]
pub async fn register_app<'r>(user: AuthenticatedUser, host: Host<'_>) -> MatePayResult<Template> {
    #[derive(Serialize)]
    struct Context<'r> {
        token: String,
        host: Option<&'r str>,
    }

    Ok(Template::render(
        "register_app",
        Context {
            token: user.token,
            host: host.0,
        },
    ))
}
