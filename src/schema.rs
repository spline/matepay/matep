// @generated automatically by Diesel CLI.

diesel::table! {
    accounts (username) {
        username -> Text,
        balance -> BigInt,
        display_name -> Nullable<Text>,
    }
}

diesel::table! {
    app_servers (app_server_id) {
        app_server_id -> Integer,
        username -> Text,
        token -> Binary,
    }
}

diesel::table! {
    items (id) {
        id -> Integer,
        name -> Text,
        price -> BigInt,
        location -> Text,
        available -> Bool,
    }
}

diesel::table! {
    payment_request_items (id) {
        id -> Integer,
        payment_request_id -> Integer,
        product_id -> Integer,
    }
}

diesel::table! {
    payment_requests (payment_request_id) {
        payment_request_id -> Integer,
        timestamp -> BigInt,
        app_server_id -> Integer,
        redirect_url -> Text,
        completed -> Bool,
    }
}

diesel::table! {
    tokens (token) {
        token -> Text,
        username -> Text,
        timestamp -> BigInt,
    }
}

diesel::table! {
    totp (username) {
        username -> Text,
        secret -> Binary,
    }
}

diesel::table! {
    transaction_items (id) {
        id -> Integer,
        transaction_id -> Integer,
        item_id -> Integer,
    }
}

diesel::table! {
    transactions (id) {
        id -> Integer,
        timestamp -> BigInt,
        sender -> Text,
        receiver -> Text,
        amount -> BigInt,
    }
}

diesel::table! {
    used_totps (token) {
        token -> Text,
        used_at -> BigInt,
    }
}

diesel::joinable!(app_servers -> accounts (username));
diesel::joinable!(payment_request_items -> items (product_id));
diesel::joinable!(payment_request_items -> payment_requests (payment_request_id));
diesel::joinable!(tokens -> accounts (username));
diesel::joinable!(totp -> accounts (username));
diesel::joinable!(transaction_items -> items (item_id));
diesel::joinable!(transaction_items -> transactions (transaction_id));

diesel::allow_tables_to_appear_in_same_query!(
    accounts,
    app_servers,
    items,
    payment_request_items,
    payment_requests,
    tokens,
    totp,
    transaction_items,
    transactions,
    used_totps,
);
