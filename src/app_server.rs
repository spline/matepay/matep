// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2023 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-only

use crate::ui::*;
use crate::{
    database, endpoints::AuthError, handle_database_error, internal_item_transfer, utils,
    AuthenticatedUser, Db, Item, MatePayResult, OAuthConfig, Transfer,
};

use rocket::{
    delete,
    form::{Form, FromForm},
    get,
    http::Status,
    post,
    request::{self, FromRequest, Outcome, Request},
    response::{status::Custom, Redirect},
    serde::{json::Json, Deserialize, Serialize},
    uri, State,
};

use rocket_dyn_templates::Template;

use log::error;

use anyhow::anyhow;

#[derive(Serialize)]
pub struct AppServer {
    id: i32,
    username: String,
    token: String,
}

impl AppServer {
    fn from_db(a: database::AppServer) -> AppServer {
        AppServer {
            id: a.id,
            username: a.username,
            token: utils::uuid_bytes_to_string(&a.token),
        }
    }
}

#[derive(Deserialize, FromForm)]
pub struct CreatePaymentRequest {
    pub product_ids: Vec<i32>,
    pub redirect_url: String,
}

#[derive(Serialize)]
pub struct PaymentRequest {
    pub payment_request_id: i32,
    pub payment_url: String,
}

#[derive(Serialize)]
pub struct PaymentCompleted {
    pub redirect_url: String,
}

#[derive(Serialize)]
pub struct PaymentStatus {
    pub completed: bool,
}

pub struct AuthenticatedAppServer {
    pub id: i32,
    pub username: String,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthenticatedAppServer {
    type Error = AuthError;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        let header = req.headers().get("MATEPAY-APP-TOKEN").next();
        match header {
            Some(token) => {
                let db = Db::from_request(req)
                    .await
                    .expect("The database fairing should have been already registered");

                let uuid_token = utils::uuid_string_to_bytes(token);
                if let Err(_e) = uuid_token {
                    return Outcome::Error((Status::BadRequest, AuthError::Unauthorized));
                }

                match db.identify_app_server(uuid_token.unwrap()).await {
                    Ok(app_server) => match app_server {
                        Some(app_server) => Outcome::Success(AuthenticatedAppServer {
                            username: app_server.username,
                            id: app_server.id,
                        }),
                        None => Outcome::Error((Status::Unauthorized, AuthError::Unauthorized)),
                    },
                    Err(error) => {
                        error!("Datenbank will nicht {:#?}", error);
                        Outcome::Error((Status::InternalServerError, AuthError::Server))
                    }
                }
            }
            None => Outcome::Error((Status::Unauthorized, AuthError::Unauthorized)),
        }
    }
}

#[post("/app_server/register")]
pub async fn register_app_server(
    db: Db,
    user: AuthenticatedUser,
) -> MatePayResult<Json<AppServer>> {
    Ok(Json(AppServer::from_db(
        db.add_app_server(user.username)
            .await
            .map_err(handle_database_error)?,
    )))
}

#[post("/app_server/add_payment_request", data = "<payment_request>")]
pub async fn add_payment_request(
    db: Db,
    oauth_config: &State<OAuthConfig>,
    app_server: AuthenticatedAppServer,
    payment_request: Json<CreatePaymentRequest>,
) -> MatePayResult<Json<PaymentRequest>> {
    if payment_request.product_ids.is_empty() {
        return Err(error_page(
            Status::UnprocessableEntity,
            Some("You need to include at least one item"),
        ));
    }

    // check products are being offered by user of the app server
    for product_id in &payment_request.product_ids {
        if !db
            .check_user_offers_product(app_server.username.clone(), *product_id)
            .await
            .map_err(handle_database_error)?
        {
            return Err(error_page(
                Status::Forbidden,
                Some("One or more selected items are not sold by the app server token owner"),
            ));
        }
    }

    // add payment_request
    let payment_request_id = db
        .add_payment_request(payment_request.into_inner(), app_server.id)
        .await
        .map_err(handle_database_error)?;

    let base_url = oauth_config.own_base_url.clone();

    Ok(Json(PaymentRequest {
        payment_request_id,
        payment_url: format!("{base_url}/payment/{payment_request_id}"),
    }))
}

#[get("/app_server/payment/<payment_request_id>/status")]
pub async fn get_payment_request_status(
    db: Db,
    app_server: AuthenticatedAppServer,
    payment_request_id: i32,
) -> MatePayResult<Json<PaymentStatus>> {
    let request = db
        .get_payment_request(payment_request_id)
        .await
        .map_err(handle_database_error)?;

    match request {
        Some(request) => {
            if request.app_server_id == app_server.id {
                Ok(Json(PaymentStatus {
                    completed: request.completed,
                }))
            } else {
                Err(error_page(
                    Status::Forbidden,
                    Some("This payment request was not created by you, and you may not access it"),
                ))
            }
        }
        None => Err(error_page(
            Status::NotFound,
            Some("Payment request not found"),
        )),
    }
}

#[delete("/app_server/payment/<payment_request_id>")]
pub async fn delete_payment_request(
    db: Db,
    payment_request_id: i32,
    app_server: AuthenticatedAppServer,
) -> MatePayResult<()> {
    let payment_request = db
        .get_payment_request(payment_request_id)
        .await
        .map_err(handle_database_error)?;

    if payment_request.is_none() {
        return Err(error_page(
            Status::NotFound,
            Some("Payment request not found"),
        ));
    }

    if payment_request.expect("Checked before").app_server_id != app_server.id {
        return Err(error_page(
            Status::Forbidden,
            Some("You are not allowed to access this payment request"),
        ));
    }

    db.delete_payment_request(payment_request_id, app_server.id)
        .await
        .map_err(handle_database_error)?;
    Ok(())
}

#[derive(Deserialize, FromForm)]
pub struct PaymentConfirmData {
    totp_token: Option<String>,
}

#[post("/payment/<payment_request_id>/confirm", data = "<form>")]
pub async fn confirm_payment_request(
    db: Db,
    user: AuthenticatedUser,
    payment_request_id: i32,
    form: Json<PaymentConfirmData>,
) -> MatePayResult<Json<PaymentCompleted>> {
    let req = db
        .get_payment_request(payment_request_id)
        .await
        .map_err(handle_database_error)?
        .ok_or(error_page(
            Status::NotFound,
            Some("Payment request not found"),
        ))?;

    if req.completed {
        return Err(error_page(
            Status::NotFound,
            Some("Payment request already completed"),
        ));
    }

    let items = db
        .get_payment_request_items(payment_request_id)
        .await
        .map_err(handle_database_error)?;

    let location = items
        .iter()
        .map(|i| i.location.to_owned())
        .reduce(|acc, e| if acc == e { acc } else { String::new() })
        .and_then(|r| if r.is_empty() { None } else { Some(r) })
        .ok_or(anyhow!("Location of items was not the same"))
        .map_err(handle_database_error)?;

    internal_item_transfer(
        &db,
        user,
        &Transfer {
            receiver: location,
            bought_items: items.iter().map(|item| item.id).collect(),
            totp_token: form.totp_token.clone(),
        },
    )
    .await?;

    db.complete_payment_request(payment_request_id)
        .await
        .map_err(handle_database_error)?;

    Ok(Json(PaymentCompleted {
        redirect_url: req.redirect_url,
    }))
}

#[get("/payment/<payment_request_id>")]
pub async fn pay(
    db: Db,
    user: AuthenticatedUser,
    payment_request_id: i32,
) -> MatePayResult<Template> {
    #[derive(Serialize)]
    struct Context {
        items: Vec<Item>,
        payment_request_id: i32,
        location: String,
        account_display_name: String,
        price: i64,
        totp_enabled: bool,
    }

    let req = db
        .get_payment_request(payment_request_id)
        .await
        .map_err(handle_database_error)?
        .ok_or(error_page(
            Status::NotFound,
            Some("Payment request not found"),
        ))?;

    if req.completed {
        return Err(error_page(
            Status::NotFound,
            Some("Payment request already completed"),
        ));
    }

    let items = db
        .get_payment_request_items_overview(payment_request_id)
        .await
        .map_err(handle_database_error)?;

    let location = items
        .iter()
        .map(|i| i.location.to_owned())
        .reduce(|acc, e| if acc == e { acc } else { String::new() })
        .and_then(|r| if r.is_empty() { None } else { Some(r) })
        .ok_or(anyhow!("Location of items was not the same"))
        .map_err(handle_database_error)?;

    let account_display_name = db
        .get_account(location.to_owned())
        .await
        .map_err(handle_database_error)?
        .display_name
        .unwrap_or(location.to_owned());

    let price: i64 = items.iter().map(|item| item.price).sum();

    let totp_enabled = db
        .totp_enabled(user.username)
        .await
        .map_err(handle_database_error)?;

    Ok(Template::render(
        "pay_request",
        Context {
            items,
            payment_request_id,
            location,
            account_display_name,
            price,
            totp_enabled,
        },
    ))
}

#[post("/payment/<payment_request_id>/confirm", data = "<form>")]
pub async fn confirm_payment(
    db: Db,
    user: AuthenticatedUser,
    payment_request_id: i32,
    form: Form<PaymentConfirmData>,
) -> MatePayResult<Redirect> {
    match confirm_payment_request(db, user, payment_request_id, Json(form.into_inner())).await {
        Ok(c) => Ok(Redirect::to(c.redirect_url.clone())),
        Err(Custom(status, body)) => match status {
            s if s == Status::Forbidden => Ok(Redirect::to(uri!(pay(payment_request_id)))),
            _ => Err(Custom(status, body)),
        },
    }
}
