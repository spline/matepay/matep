// SPDX-FileCopyrightText: 2023 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

use anyhow::{anyhow, Result};
use uuid::Uuid;

pub fn uuid_bytes_to_string(token: &[u8]) -> String {
    Uuid::from_slice(token).unwrap().to_string()
}

pub fn uuid_string_to_bytes(token: &str) -> Result<Vec<u8>> {
    Ok(Uuid::try_parse(token).map(|uuid| uuid.into_bytes().to_vec())?)
}

pub fn parse_euro_to_cent(string: &str) -> Result<i64> {
    let comma_location = match string.find('.').or(string.find(',')) {
        None => return Ok(string.parse::<i64>()? * 100),
        Some(idx) => idx,
    };

    let start;
    let sign;
    if string.starts_with('-') {
        start = 1;
        sign = -1;
    } else {
        start = 0;
        sign = 1;
    }

    let euro_part: &str = &string[start..comma_location];
    let cent_part = &string[comma_location + 1..string.len()];
    if cent_part.len() > 2 {
        return Err(anyhow!("More than two digits after the comma"));
    }
    let mut cents = euro_part.parse::<u32>()? as i64 * 100;

    let mut scale = 10;
    for digit in cent_part.chars() {
        cents += digit.to_digit(10).ok_or(anyhow!("Non-digit in string"))? as i64 * scale;
        scale /= 10;
    }

    Ok(sign * cents)
}

#[test]
fn parse_euro_cent() {
    let cents = parse_euro_to_cent("1312.13").unwrap();
    assert_eq!(cents, 131213)
}

#[test]
fn parse_euro_cent_incomplete() {
    let cents = parse_euro_to_cent("2.5").unwrap();
    assert_eq!(cents, 250);
}

#[test]
fn parse_euro_cent_trailing_zero() {
    let cents = parse_euro_to_cent("2.50").unwrap();
    assert_eq!(cents, 250);
}

#[test]
fn parse_euro_cent_error() {
    let cents = parse_euro_to_cent("1312.1312");
    assert!(cents.is_err());
}

#[test]
fn parse_euro_cent_no_fraction() {
    let cents = parse_euro_to_cent("161").unwrap();
    assert_eq!(cents, 16100);
}

#[test]
fn parse_euro_cent_negative() {
    let cents = parse_euro_to_cent("-161").unwrap();
    assert_eq!(cents, -16100);
}

#[test]
fn parse_euro_cent_comma() {
    let cents = parse_euro_to_cent("161,01").unwrap();
    assert_eq!(cents, 16101);
}

#[test]
fn parse_euro_cent_two_comma() {
    let cents = parse_euro_to_cent("161,0,");
    assert!(cents.is_err())
}

#[test]
fn parse_euro_cent_comma_point() {
    let cents = parse_euro_to_cent("161.0,");
    assert!(cents.is_err())
}

#[test]
fn parse_euro_cent_negative_fraction() {
    let cents = parse_euro_to_cent("-161,01").unwrap();
    assert_eq!(cents, -16101);
}

#[test]
fn parse_cent() {
    let cents = parse_euro_to_cent("0,10").unwrap();
    assert_eq!(cents, 10);
}
