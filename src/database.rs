// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2022 Abraham Söyler <brahms@spline.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

/*
 *  database connection code
 *
 * */

use diesel::{dsl::count, prelude::*};
use rocket_sync_db_pools::database;
use uuid::Uuid;

use crate::schema::*;
// Items that are stored identically in the database to how they are serialized
use crate::app_server::CreatePaymentRequest;
use crate::endpoints::{Account, Item, Location};

use crate::now_in_unix;

#[derive(Insertable)]
pub struct Token {
    pub token: String,
    pub username: String,
    pub timestamp: i64,
}

#[derive(Insertable)]
#[diesel(table_name = accounts)]
pub struct InsertableAccount {
    username: String,
    balance: i64,
    display_name: String,
}

#[derive(Queryable)]
pub struct Transaction {
    pub id: i32,
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
}

// Without id, as the id will be decided by the database
#[derive(Insertable)]
#[diesel(table_name = transactions)]
pub struct InsertableTransaction {
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
}

#[derive(Insertable)]
#[diesel(table_name = items)]
pub struct InsertableItem {
    pub name: String,
    pub price: i64,
    pub location: String,
    pub available: bool,
}

#[derive(Insertable)]
pub struct TransactionItem {
    transaction_id: i32,
    item_id: i32,
}

#[derive(Queryable)]
pub struct AppServer {
    pub id: i32,
    pub username: String,
    pub token: Vec<u8>,
}

#[derive(Insertable)]
#[diesel(table_name = app_servers)]
pub struct InsertableAppServer {
    username: String,
    token: Vec<u8>,
}

#[derive(Queryable)]
pub struct PaymentRequest {
    pub id: i32,
    pub timestamp: i64,
    pub app_server_id: i32,
    pub redirect_url: String,
    pub completed: bool,
}

#[derive(Insertable)]
#[diesel(table_name = payment_requests)]
pub struct InsertablePaymentRequest {
    pub timestamp: i64,
    pub app_server_id: i32,
    pub redirect_url: String,
    pub completed: bool,
}

#[derive(Insertable)]
#[diesel(table_name = payment_request_items)]
pub struct InsertablePaymentRequestItem {
    payment_request_id: i32,
    product_id: i32,
}

#[derive(Insertable, Queryable)]
#[diesel(table_name = totp)]
pub struct Totp {
    pub username: String,
    pub secret: Vec<u8>,
}

#[database("matepay")]
pub struct Db(diesel::SqliteConnection);

impl Db {
    pub async fn create_account(&self, username: String, display_name: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(accounts::table)
                .values(InsertableAccount {
                    username,
                    balance: 0,
                    display_name,
                })
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn check_account_exists(&self, username: String) -> QueryResult<bool> {
        self.run(move |c| {
            accounts::table
                .filter(accounts::username.eq(username))
                .select(count(accounts::username).gt(0))
                .first::<bool>(c)
        })
        .await
    }

    pub async fn add_account_display_name(
        &self,
        username: String,
        display_name: String,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(accounts::table)
                .filter(accounts::username.eq(username))
                .set(accounts::display_name.eq(display_name))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_account(&self, username: String) -> QueryResult<Account> {
        self.run(move |c| {
            accounts::table
                .filter(accounts::username.eq(username))
                .first(c)
        })
        .await
    }

    pub async fn get_accounts(&self) -> QueryResult<Vec<Account>> {
        self.run(move |c| accounts::table.load(c)).await
    }

    pub async fn get_all_items(&self, location: String) -> QueryResult<Vec<Item>> {
        self.run(move |c| items::table.filter(items::location.eq(location)).load(c))
            .await
    }

    pub async fn get_item(&self, item_id: i32) -> QueryResult<Item> {
        self.run(move |c| items::table.filter(items::id.eq(item_id)).first(c))
            .await
    }

    pub async fn get_transactions_for_user(
        &self,
        username: String,
    ) -> QueryResult<Vec<Transaction>> {
        self.run(move |c| {
            transactions::table
                .filter(
                    transactions::sender
                        .eq(username.clone())
                        .or(transactions::receiver.eq(username)),
                )
                .order_by(transactions::timestamp.desc())
                .load(c)
        })
        .await
    }

    pub async fn get_transactions(&self) -> QueryResult<Vec<Transaction>> {
        self.run(move |c| {
            transactions::table
                .order_by(transactions::timestamp.asc())
                .load(c)
        })
        .await
    }

    pub async fn get_transaction_items(&self, transaction_id: i32) -> QueryResult<Vec<i32>> {
        self.run(move |c| {
            transaction_items::table
                .filter(transaction_items::transaction_id.eq(transaction_id))
                .select(transaction_items::item_id)
                .load(c)
        })
        .await
    }

    pub async fn transfer(
        &self,
        from_account: String,
        to_account: String,
        amount: i64,
        items: Vec<i32>,
    ) -> QueryResult<()> {
        self.run(move |c| {
            // Run the queries to add money to the receivers account and to remove money from the senders account
            // in a transaction, so they are stored in one atomic database operation,
            // to make sure the overall amount of money stays the same
            c.transaction(move |conn| -> QueryResult<()> {
                let updated_rows = diesel::update(accounts::table)
                    .filter(accounts::username.eq(&to_account))
                    .set(accounts::balance.eq(accounts::balance + amount))
                    .execute(conn)?;
                assert_eq!(updated_rows, 1);

                let updated_rows = diesel::update(accounts::table)
                    .filter(accounts::username.eq(&from_account))
                    .set(accounts::balance.eq(accounts::balance - amount))
                    .execute(conn)?;
                assert_eq!(updated_rows, 1);

                // Archive transaction
                let transaction_id = diesel::insert_into(transactions::table)
                    .values(InsertableTransaction {
                        timestamp: now_in_unix(),
                        sender: from_account,
                        receiver: to_account,
                        amount,
                    })
                    .returning(transactions::id)
                    .get_result::<i32>(conn)?;

                for item_id in items {
                    diesel::insert_into(transaction_items::table)
                        .values(TransactionItem {
                            item_id,
                            transaction_id,
                        })
                        .execute(conn)?;
                }

                Ok(())
            })
        })
        .await?;
        Ok(())
    }

    pub async fn add_item(&self, name: String, price: i64, location: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(items::table)
                .values(InsertableItem {
                    name,
                    price,
                    location,
                    available: true,
                })
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn update_availability(
        &self,
        id: i32,
        location: String,
        available: bool,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(items::table)
                .filter(items::id.eq(id).and(items::location.eq(location)))
                .set(items::available.eq(available))
                .execute(c)
        })
        .await?;
        Ok(())
    }
    pub async fn update_item_price(
        &self,
        id: i32,
        location: String,
        price: i64,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(items::table)
                .filter(items::id.eq(id).and(items::location.eq(location)))
                .set(items::price.eq(price))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn check_user_offers_product(
        &self,
        username: String,
        product_id: i32,
    ) -> QueryResult<bool> {
        self.get_item(product_id)
            .await
            .map(|item| item.location == username)
    }

    pub async fn locations(&self) -> QueryResult<Vec<Location>> {
        let locations = self
            .run(move |c| {
                items::table
                    .distinct()
                    .inner_join(accounts::table.on(items::location.eq(accounts::username)))
                    .select((items::location, accounts::display_name))
                    .load::<Location>(c)
            })
            .await?;
        Ok(locations)
    }

    pub async fn add_app_server(&self, username: String) -> QueryResult<AppServer> {
        self.run(|c| {
            diesel::insert_into(app_servers::table)
                .values(InsertableAppServer {
                    username,
                    token: Uuid::new_v4().as_bytes().to_vec(),
                })
                .returning((
                    app_servers::app_server_id,
                    app_servers::username,
                    app_servers::token,
                ))
                .get_result::<AppServer>(c)
        })
        .await
    }

    pub async fn identify_app_server(&self, token: Vec<u8>) -> QueryResult<Option<AppServer>> {
        self.run(move |c| {
            app_servers::table
                .filter(app_servers::token.eq(token))
                .select((
                    app_servers::app_server_id,
                    app_servers::username,
                    app_servers::token,
                ))
                .first(c)
                .optional()
        })
        .await
    }

    pub async fn add_payment_request(
        &self,
        payment_request: CreatePaymentRequest,
        app_server_id: i32,
    ) -> QueryResult<i32> {
        let payment_request_id = self
            .run(move |c| {
                diesel::insert_into(payment_requests::table)
                    .values(InsertablePaymentRequest {
                        timestamp: now_in_unix(),
                        app_server_id,
                        redirect_url: payment_request.redirect_url,
                        completed: false,
                    })
                    .returning(payment_requests::payment_request_id)
                    .get_result(c)
            })
            .await?;

        self.run(move |c| -> QueryResult<()> {
            for item_id in payment_request.product_ids {
                diesel::insert_into(payment_request_items::table)
                    .values(&InsertablePaymentRequestItem {
                        payment_request_id,
                        product_id: item_id,
                    })
                    .execute(c)?;
            }
            Ok(())
        })
        .await?;

        Ok(payment_request_id)
    }

    pub async fn delete_payment_request(
        &self,
        payment_request_id: i32,
        app_server_id: i32,
    ) -> QueryResult<()> {
        self.run(move |c| {
            diesel::delete(payment_request_items::table)
                .filter(payment_request_items::payment_request_id.eq(payment_request_id))
                .execute(c)?;

            diesel::delete(payment_requests::table)
                .filter(
                    payment_requests::app_server_id
                        .eq(app_server_id)
                        .and(payment_requests::payment_request_id.eq(payment_request_id)),
                )
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn get_payment_request(
        &self,
        request_id: i32,
    ) -> QueryResult<Option<PaymentRequest>> {
        self.run(move |c| {
            use payment_requests::*;
            payment_requests::table
                .filter(payment_requests::payment_request_id.eq(request_id))
                .select((
                    payment_request_id,
                    timestamp,
                    app_server_id,
                    redirect_url,
                    completed,
                ))
                .first::<PaymentRequest>(c)
                .optional()
        })
        .await
    }

    pub async fn get_payment_request_items(
        &self,
        payment_request_id: i32,
    ) -> QueryResult<Vec<Item>> {
        self.run(move |c| {
            payment_request_items::table
                .filter(payment_request_items::payment_request_id.eq(payment_request_id))
                .inner_join(items::table)
                .select((
                    items::id,
                    items::name,
                    items::price,
                    items::location,
                    items::available,
                ))
                .load::<Item>(c)
        })
        .await
    }

    /// Returns the items, grouped by the same item type. Only use for displaying to the user.
    pub async fn get_payment_request_items_overview(
        &self,
        payment_request_id: i32,
    ) -> QueryResult<Vec<Item>> {
        self.run(move |c| {
            payment_request_items::table
                .filter(payment_request_items::payment_request_id.eq(payment_request_id))
                .inner_join(items::table)
                .group_by(items::id)
                .select((
                    items::id,
                    items::name,
                    (count(items::price).assume_not_null() * items::price),
                    items::location,
                    items::available,
                ))
                .load::<Item>(c)
        })
        .await
    }

    pub async fn complete_payment_request(&self, payment_request_id: i32) -> QueryResult<()> {
        self.run(move |c| {
            diesel::update(payment_requests::table)
                .filter(payment_requests::payment_request_id.eq(payment_request_id))
                .set(payment_requests::completed.eq(true))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn set_totp_secret(&self, username: String, secret: Vec<u8>) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(totp::table)
                .values(Totp { username, secret })
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn totp_enabled(&self, username: String) -> QueryResult<bool> {
        self.run(move |c| {
            totp::table
                .filter(totp::username.eq(username))
                .select(count(totp::username).gt(0))
                .get_result(c)
        })
        .await
    }

    pub async fn totp_secret(&self, username: String) -> QueryResult<Totp> {
        self.run(move |c| {
            totp::table
                .filter(totp::username.eq(username))
                .get_result(c)
        })
        .await
    }

    pub async fn mark_totp_used(&self, token: String) -> QueryResult<()> {
        self.run(move |c| {
            diesel::insert_into(used_totps::table)
                .values((
                    used_totps::token.eq(token),
                    used_totps::used_at.eq(now_in_unix()),
                ))
                .execute(c)
        })
        .await?;
        Ok(())
    }

    pub async fn is_totp_used(&self, token: String) -> QueryResult<bool> {
        let expire_time = now_in_unix() - 60; // seconds
        self.run(move |c| {
            diesel::delete(used_totps::table)
                .filter(used_totps::used_at.lt(expire_time))
                .execute(c)
        })
        .await?;

        self.run(move |c| {
            used_totps::table
                .filter(used_totps::token.eq(token))
                .select(count(used_totps::token).gt(0))
                .get_result(c)
        })
        .await
    }
}
