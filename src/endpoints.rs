// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2022 Abraham Söyler <brahms@spline.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

/*
 *  contains all endpoints for the MatePay API
 *
 * */

use rocket::{
    form::FromForm,
    get,
    http::Status,
    patch, post,
    request::{self, FromRequest, Outcome, Request},
    response::status::Custom,
    serde::{json::Json, Deserialize, Serialize},
    Responder,
};

use totp_rs::{Algorithm, Secret, TOTP};

use diesel::result::DatabaseErrorKind;
use diesel::result::Error as DieselError;

use crate::{database::Db, ui, utils, MATEPAY_CENTRAL_BANK_USER};

use std::{collections::HashMap, fmt::Debug};

use log::{error, warn};

use spline_oauth::routes::AuthenticatedUser;

#[derive(Debug)]
pub enum AuthError {
    Unauthorized,
    Server,
    DisallowedUser,
}

pub struct CentralBankUser {
    pub user: AuthenticatedUser,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for CentralBankUser {
    type Error = AuthError;

    async fn from_request(req: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        match AuthenticatedUser::from_request(req).await {
            Outcome::Success(AuthenticatedUser { username, token })
                if username == MATEPAY_CENTRAL_BANK_USER =>
            {
                Outcome::Success(CentralBankUser {
                    user: AuthenticatedUser { username, token },
                })
            }
            Outcome::Success(AuthenticatedUser { username, token: _ }) => {
                warn!(
                    "Invalid user {} tried to access central bank interface",
                    username
                );
                Outcome::Error((Status::Forbidden, AuthError::DisallowedUser))
            }
            Outcome::Error(e) => {
                let (status, _) = e;
                Outcome::Error((status, AuthError::Unauthorized))
            }
            _ => {
                unreachable!(
                    "The AuthenticatedUser guard either returns Failur or Success, never Forward"
                )
            }
        }
    }
}

#[derive(Serialize, Responder)]
#[response(status = 201)]
pub struct Token {
    pub token: String,
}

#[derive(Serialize, Queryable)]
pub struct Account {
    pub username: String,
    pub balance: i64,
    pub display_name: Option<String>,
}

#[derive(Serialize, Queryable, PartialEq)]
pub struct Item {
    pub id: i32,
    pub name: String,
    pub price: i64,
    pub location: String,
    pub available: bool,
}

#[derive(Serialize)]
pub struct Transaction {
    pub timestamp: i64,
    pub sender: String,
    pub receiver: String,
    pub amount: i64,
    pub items: Vec<i32>,
}

#[derive(Deserialize)]
pub struct Transfer {
    pub receiver: String,
    pub bought_items: Vec<i32>,
    pub totp_token: Option<String>,
}

#[derive(Deserialize, FromForm)]
pub struct AddItemRequest {
    // id will be determined by database
    name: String,
    price: String,
    // location will be determined by user who sends the request
}

#[derive(Deserialize, FromForm)]
pub struct UpdateItemRequest {
    price: String,
    available: bool,
}

#[derive(Serialize, Queryable)]
pub struct Location {
    pub name: String,
    pub display_name: Option<String>,
}

#[derive(Deserialize, FromForm)]
pub struct CentralBankTransfer {
    to_account: String,
    amount: String,
    totp_token: Option<String>,
}

#[derive(Deserialize, FromForm)]
pub struct PayoutRequest {
    from_account: String,
    totp_token: Option<String>,
}

// routes

pub fn handle_database_error<T: Debug>(error: T) -> Custom<rocket_dyn_templates::Template> {
    error!("A database query failed: {:?}", error);
    ui::error_page(
        Status::InternalServerError,
        Some("A database query failed. More info has been logged on the server side."),
    )
}

pub type MatePayResult<T> = Result<T, Custom<rocket_dyn_templates::Template>>;

#[post("/logout")]
pub async fn logout(db: spline_oauth::db::Db, user: AuthenticatedUser) -> MatePayResult<()> {
    db.delete_tokens(user.username)
        .await
        .map_err(handle_database_error)
}

#[get("/account")]
pub async fn account(db: Db, user: AuthenticatedUser) -> MatePayResult<Json<Account>> {
    db.get_account(user.username)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[derive(Serialize)]
pub struct TotpSetupResult {
    pub uri: String,
}

#[post("/totp")]
pub async fn enable_totp(db: Db, user: AuthenticatedUser) -> MatePayResult<Json<TotpSetupResult>> {
    let secret = Secret::generate_secret();
    let totp = TOTP::new(
        Algorithm::SHA1,
        6,  // digits
        1,  // skew
        30, // step
        secret.to_bytes().unwrap(),
        Some("MatePay".to_string()),
        user.username.clone(),
    )
    .unwrap();

    let raw_secret = secret
        .to_bytes()
        .expect("The secret was just generated and is a valid representation");
    let db_result = db.set_totp_secret(user.username, raw_secret).await;

    // Disallow resetting the totp secret
    match db_result {
        Err(DieselError::DatabaseError(DatabaseErrorKind::UniqueViolation, _)) => {
            Err(ui::error_page(
                Status::Forbidden,
                Some("Resetting the totp token is disallowed"),
            ))
        }
        Err(e) => Err(handle_database_error(e)),
        Ok(()) => Ok(Json(TotpSetupResult {
            uri: totp.get_url(),
        })),
    }
}

#[get("/items/<location>")]
pub async fn items(db: Db, location: String) -> MatePayResult<Json<Vec<Item>>> {
    db.get_all_items(location)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[get("/item/<item_id>")]
pub async fn item(db: Db, item_id: i32) -> MatePayResult<Json<Item>> {
    db.get_item(item_id)
        .await
        .map_err(handle_database_error)
        .map(Json)
}

#[get("/transactions")]
pub async fn transactions(
    db: Db,
    user: AuthenticatedUser,
) -> MatePayResult<Json<Vec<Transaction>>> {
    let transactions = db
        .get_transactions_for_user(user.username)
        .await
        .map_err(handle_database_error)?;

    let mut out = Vec::new();

    // This needs database optimization
    for transaction in transactions {
        out.push(Transaction {
            timestamp: transaction.timestamp,
            sender: transaction.sender,
            receiver: transaction.receiver,
            amount: transaction.amount,
            items: db
                .get_transaction_items(transaction.id)
                .await
                .map_err(handle_database_error)?,
        })
    }

    Ok(Json(out))
}

pub async fn redeem_totp_code(
    db: &Db,
    user: &AuthenticatedUser,
    token: &Option<String>,
) -> MatePayResult<()> {
    if db
        .totp_enabled(user.username.clone())
        .await
        .map_err(handle_database_error)?
    {
        match token {
            None => Err(ui::error_page(
                Status::Forbidden,
                Some("You must supply a totp token"),
            )),
            Some(token) => {
                let secret = db
                    .totp_secret(user.username.clone())
                    .await
                    .map_err(handle_database_error)?;
                let totp = TOTP::new(
                    Algorithm::SHA1,
                    6,
                    1,
                    30,
                    secret.secret,
                    None,
                    user.username.clone(),
                )
                .map_err(handle_database_error)?;

                let token_crypt_valid = totp.check_current(token).map_err(handle_database_error)?;
                let token_still_valid = !db
                    .is_totp_used(token.clone())
                    .await
                    .map_err(handle_database_error)?;
                let valid = token_crypt_valid && token_still_valid;

                if !valid {
                    Err(ui::error_page(
                        Status::Forbidden,
                        Some("The totp token was invalid"),
                    ))
                } else {
                    db.mark_totp_used(token.clone())
                        .await
                        .map_err(handle_database_error)?;
                    Ok(())
                }
            }
        }
    } else {
        Ok(())
    }
}

pub async fn internal_transfer(
    db: &Db,
    user: &AuthenticatedUser,
    receiver: String,
    amount: i64,
    token: &Option<String>,
    bought_items: Vec<i32>,
) -> MatePayResult<()> {
    redeem_totp_code(db, user, token).await?;

    if amount.is_negative() {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("Amount to transfer must be positive"),
        ));
    }

    assert_eq!(
        internal_audit(db, user).await.unwrap().state,
        AuditState::Ok
    );

    let sender = user.username.clone();

    let receiver_balance = db
        .get_account(receiver.clone())
        .await
        .map_err(handle_database_error)?
        .balance;

    if receiver_balance.checked_add(amount).unwrap() < 0 && receiver != MATEPAY_CENTRAL_BANK_USER {
        return Err(ui::default_error_page(Status::PaymentRequired));
    }

    let sender_balance = db
        .get_account(sender.clone())
        .await
        .map_err(handle_database_error)?
        .balance;

    if amount > sender_balance {
        return Err(ui::error_page(
            Status::PaymentRequired,
            Some("Your account balance is not high enough to continue"),
        ));
    }

    // transfer the money
    db.transfer(sender.clone(), receiver.clone(), amount, bought_items)
        .await
        .map_err(handle_database_error)?;

    assert_eq!(
        internal_audit(db, user).await.unwrap().state,
        AuditState::Ok
    );

    Ok(())
}

pub async fn internal_item_transfer(
    db: &Db,
    user: AuthenticatedUser,
    transfer: &Transfer,
) -> MatePayResult<()> {
    // Check that transaction contains at least one item
    if transfer.bought_items.is_empty() {
        return Err(ui::error_page(
            Status::UnprocessableEntity,
            Some("You must select at least one item"),
        ));
    }

    // Retrieve all items
    let mut items = Vec::new();
    for item_id in &transfer.bought_items {
        match db.get_item(*item_id).await {
            Ok(item) => {
                if item.location != transfer.receiver {
                    return Err(ui::error_page(
                        Status::Forbidden,
                        Some("The requested item cannot be bought from the selected receiver"),
                    ));
                }
                if item.price < 0 {
                    error!("Refusing purchase of item {item_id} which has negative price");
                    return Err(ui::error_page(
                        Status::Forbidden,
                        Some("items to buy cannot have a negative price"),
                    ));
                }

                items.push(item)
            }
            Err(DieselError::NotFound) => {
                return Err(ui::error_page(
                    Status::UnprocessableEntity,
                    Some("A selected item was not found"),
                ))
            }
            Err(e) => return Err(handle_database_error(e)),
        }
    }

    // calculate overall price for the items
    let amount = items.iter().map(|item| item.price).sum();

    internal_transfer(
        db,
        &user,
        transfer.receiver.clone(),
        amount,
        &transfer.totp_token,
        transfer.bought_items.clone(),
    )
    .await
}

#[post("/transfer", data = "<transfer>")]
pub async fn transfer(
    db: Db,
    user: AuthenticatedUser,
    transfer: Json<Transfer>,
) -> MatePayResult<()> {
    internal_item_transfer(&db, user, &transfer).await
}

#[derive(Serialize, Deserialize, FromForm)]
pub struct DirectTransfer {
    totp_token: Option<String>,
    receiver: String,
    amount: String,
}

#[post("/direct-transfer", data = "<transfer>")]
pub async fn direct_transfer(
    db: Db,
    user: AuthenticatedUser,
    transfer: Json<DirectTransfer>,
) -> MatePayResult<()> {
    match db.get_account(transfer.receiver.clone()).await {
        Err(DieselError::NotFound) => {
            return Err(ui::error_page(
                Status::UnprocessableEntity,
                Some("The receiver account does not exist"),
            ))
        }
        Err(e) => return Err(handle_database_error(e)),
        Ok(_) => {}
    };

    let amount = utils::parse_euro_to_cent(&transfer.amount)
        .map_err(|_| ui::error_page(Status::UnprocessableEntity, Some("Invalid amount syntax")))?;

    if amount.is_negative() {
        return Err(ui::error_page(
            Status::UnprocessableEntity,
            Some("Amount can not be negative"),
        ));
    }

    internal_transfer(
        &db,
        &user,
        transfer.receiver.clone(),
        amount,
        &transfer.totp_token,
        Vec::new(),
    )
    .await
}

#[post("/items", data = "<item>")]
pub async fn add_item(
    db: Db,
    user: AuthenticatedUser,
    item: Json<AddItemRequest>,
) -> MatePayResult<()> {
    let price = utils::parse_euro_to_cent(&item.price)
        .map_err(|_| ui::error_page(Status::UnprocessableEntity, Some("Invalid price syntax")))?;
    if price.is_negative() {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("Price can not be negative"),
        ));
    }

    db.add_item(item.name.clone(), price, user.username)
        .await
        .map_err(handle_database_error)?;
    Ok(())
}

#[patch("/item/<id>", data = "<item>")]
pub async fn update_item(
    db: Db,
    user: AuthenticatedUser,
    id: i32,
    item: Json<UpdateItemRequest>,
) -> MatePayResult<()> {
    if !db
        .check_user_offers_product(user.username.clone(), id)
        .await
        .map_err(handle_database_error)?
    {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("You are not allowed to edit this item"),
        ));
    }

    let price = utils::parse_euro_to_cent(&item.price)
        .map_err(|_| ui::error_page(Status::UnprocessableEntity, Some("Invalid price syntax")))?;

    if price.is_negative() {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("Price can not be negative"),
        ));
    }

    db.update_item_price(id, user.username.clone(), price)
        .await
        .map_err(handle_database_error)?;
    db.update_availability(id, user.username.clone(), item.available)
        .await
        .map_err(handle_database_error)?;
    Ok(())
}

#[get("/locations")]
pub async fn locations(db: Db) -> MatePayResult<Json<Vec<Location>>> {
    let locations = db.locations().await.map_err(handle_database_error)?;
    Ok(Json(locations))
}

///
/// This function does arbitrary transfers from the bank to someone else.
/// It doesn't do any checks on its own, so it should only be used after
/// checking preconditions of the intended use case.
///
pub async fn central_bank_transfer(
    db: Db,
    bank: CentralBankUser,
    to_account: &str,
    amount: i64,
    totp_token: &Option<String>,
) -> MatePayResult<()> {
    redeem_totp_code(&db, &bank.user, totp_token).await?;

    assert_eq!(
        internal_audit(&db, &bank.user).await.unwrap().state,
        AuditState::Ok
    );

    let account = match db.get_account(to_account.to_string()).await {
        Err(DieselError::NotFound) => {
            return Err(ui::error_page(
                Status::UnprocessableEntity,
                Some("Receiver account not found"),
            ))
        }
        Err(e) => return Err(handle_database_error(e)),
        Ok(account) => account,
    };

    if (account.balance + amount).is_negative() {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("Target account should have a positive balance after the transaction"),
        ));
    }

    let result = db
        .transfer(
            bank.user.username.clone(),
            to_account.to_string(),
            amount,
            Vec::new(),
        )
        .await
        .map_err(handle_database_error);

    assert_eq!(
        internal_audit(&db, &bank.user).await.unwrap().state,
        AuditState::Ok
    );

    result
}

#[post("/deposit", data = "<transfer>")]
pub async fn deposit(
    db: Db,
    bank: CentralBankUser,
    transfer: Json<CentralBankTransfer>,
) -> MatePayResult<()> {
    let amount = utils::parse_euro_to_cent(&transfer.amount)
        .map_err(|_| ui::error_page(Status::UnprocessableEntity, Some("Invalid amount syntax")))?;

    if amount.is_negative() {
        return Err(ui::error_page(
            Status::Forbidden,
            Some("Amount must be negative"),
        ));
    }

    central_bank_transfer(db, bank, &transfer.to_account, amount, &transfer.totp_token).await
}

#[derive(Serialize)]
pub struct PayoutResult {
    pub amount: i64,
}

#[post("/payout", data = "<transfer>")]
pub async fn payout(
    db: Db,
    bank: CentralBankUser,
    transfer: Json<PayoutRequest>,
) -> MatePayResult<Json<PayoutResult>> {
    let account = db
        .get_account(transfer.from_account.clone())
        .await
        .map_err(handle_database_error)?;

    let amount = -account.balance;
    assert!(amount.is_negative());

    central_bank_transfer(
        db,
        bank,
        &transfer.from_account,
        amount,
        &transfer.totp_token,
    )
    .await?;

    Ok(Json(PayoutResult {
        amount: account.balance,
    }))
}

#[derive(Serialize, Debug, PartialEq, Eq)]
pub enum AuditState {
    Ok,
    Inconsistent,
}

#[derive(Serialize)]
pub struct Audit {
    capitalization: i64,
    state: AuditState,
}

pub async fn internal_audit(db: &Db, _user: &AuthenticatedUser) -> MatePayResult<Json<Audit>> {
    // Check that total balance adds up to zero
    let mut total_users_amount = 0;
    let users = db.get_accounts().await.map_err(handle_database_error)?;
    for user in users {
        total_users_amount += user.balance;
    }

    // Total amount should be 0, as the central bank account has all money it has given out as negative balance.

    // Check if balances calculated from transactions add up with what we have stored
    let mut account_balances = HashMap::<String, i64>::new();
    let transactions = db.get_transactions().await.map_err(handle_database_error)?;
    for transaction in transactions {
        // Add money to receiver account
        let balance = account_balances.get(&transaction.receiver).unwrap_or(&0);
        account_balances.insert(transaction.receiver, *balance + transaction.amount);

        // Remove money from sender account
        let balance = account_balances.get(&transaction.sender).unwrap_or(&0);
        account_balances.insert(transaction.sender, *balance - transaction.amount);
    }

    let mut db_consistent = true;
    for (username, balance) in account_balances {
        let db_balance = db
            .get_account(username)
            .await
            .map_err(handle_database_error)?
            .balance;
        db_consistent &= db_balance == balance;
    }

    // Calculate the total amount of money given out
    let capitalization = -db
        .get_account(MATEPAY_CENTRAL_BANK_USER.to_string())
        .await
        .map_err(handle_database_error)?
        .balance;

    Ok(Json(Audit {
        capitalization,
        state: if total_users_amount == 0 && db_consistent {
            AuditState::Ok
        } else {
            AuditState::Inconsistent
        },
    }))
}

#[get("/audit")]
pub async fn audit(db: Db, user: AuthenticatedUser) -> MatePayResult<Json<Audit>> {
    internal_audit(&db, &user).await
}
