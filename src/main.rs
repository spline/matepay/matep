// SPDX-FileCopyrightText: 2023 Jonah Brüchert <jbb@kaidan.im>
// SPDX-FileCopyrightText: 2022 Abraham Söyler <brahms@spline.de>
//
// SPDX-License-Identifier: AGPL-3.0-only

mod app_server;
mod database;
mod endpoints;
mod schema;
mod templates;
mod ui;
mod utils;

use std::time::SystemTime;

use rocket::{catchers, fs::FileServer, launch, routes};

use crate::templates::TemplateExtensions;

use spline_oauth::{routes::AuthenticatedUser, OAuthConfig, OAuthSupport};

#[macro_use]
extern crate diesel;

use crate::database::Db;

use crate::endpoints::*;

const MATEPAY_CENTRAL_BANK_USER: &str = "mzb";

pub fn now_in_unix() -> i64 {
    SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .try_into()
        .expect("Value doesn't fit into i64 anymore. Time to upgrade!")
}

#[launch]
async fn rocket() -> _ {
    pretty_env_logger::init();

    let rocket = rocket::build();
    let figment = rocket.figment();
    let oauth_config: OAuthConfig = figment
        .extract_inner("oauth")
        .expect("Reading oauth config");

    rocket
        .attach(OAuthSupport::fairing(oauth_config))
        .attach(Db::fairing())
        .attach(TemplateExtensions::fairing())
        .register(
            "/",
            catchers![ui::catch_payment_required, ui::catch_forbidden],
        )
        .mount("/css", FileServer::from("css"))
        .mount("/img", FileServer::from("img"))
        .mount("/fonts", FileServer::from("fonts"))
        .mount(
            "/",
            routes![
                ui::index,
                ui::home,
                ui::transactions,
                ui::do_transfer,
                ui::get_items,
                ui::locations,
                ui::add_item,
                ui::do_add_item,
                ui::update_item,
                ui::do_update_item,
                ui::payment_required,
                ui::deposit,
                ui::payout,
                ui::do_deposit,
                ui::do_payout,
                ui::totp,
                ui::setup_totp,
                ui::do_direct_transfer,
                ui::direct_transfer,
                ui::settings,
                ui::register_app,
                app_server::pay,
                app_server::confirm_payment,
            ],
        )
        .mount(
            "/api/v1/",
            routes![
                logout,
                account,
                items,
                transactions,
                item,
                transfer,
                add_item,
                update_item,
                locations,
                audit,
                deposit,
                payout,
                enable_totp,
                direct_transfer,
                app_server::register_app_server,
                app_server::add_payment_request,
                app_server::confirm_payment_request,
                app_server::get_payment_request_status,
                app_server::delete_payment_request,
            ],
        )
}
