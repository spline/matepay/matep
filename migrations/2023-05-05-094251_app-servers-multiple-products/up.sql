CREATE TABLE payment_request_items (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    payment_request_id INTEGER NOT NULL,
    product_id INTEGER NOT NULL,

    FOREIGN KEY (payment_request_id) REFERENCES payment_requests(payment_request_id)
    FOREIGN KEY(product_id) REFERENCES items(id)
);

CREATE TABLE payment_requests2 (
    payment_request_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    timestamp BIGINT NOT NULL,
    app_server_id INTEGER NOT NULL,
    redirect_url TEXT NOT NULL,
    completed BOOLEAN NOT NULL
);

INSERT INTO payment_requests2 SELECT payment_request_id, timestamp, app_server_id, redirect_url, completed FROM payment_requests;
DROP TABLE payment_requests;
ALTER TABLE payment_requests2 RENAME TO payment_requests;
