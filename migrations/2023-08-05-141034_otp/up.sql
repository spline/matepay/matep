CREATE TABLE totp (
    username TEXT NOT NULL PRIMARY KEY,
    secret BLOB NOT NULL,
    FOREIGN KEY (username) REFERENCES accounts(username)
);
