CREATE TABLE app_servers (
    app_server_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    username TEXT NOT NULL,
    token BLOB NOT NULL,
    FOREIGN KEY(username) REFERENCES accounts (username)
);

CREATE TABLE payment_requests (
    payment_request_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    timestamp BIGINT NOT NULL,
    app_server_id INTEGER NOT NULL,
    redirect_url TEXT NOT NULL,
    product_id INTEGER NOT NULL,
    completed BOOLEAN NOT NULL,
    FOREIGN KEY(product_id) REFERENCES items(id)
);
