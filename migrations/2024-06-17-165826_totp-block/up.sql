CREATE TABLE used_totps (
    token TEXT PRIMARY KEY NOT NULL,
    used_at BIGINT NOT NULL
);
